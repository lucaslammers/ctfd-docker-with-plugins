# Using ChatGPT for plugin unit-tests

## For example: I want to add unit-tests for the class `CTFdRegexFlag` from the plugin `flags`

My prompt would be as follows:

```txt
Hello! I have a Python class named CTFdRegexFlag from the CTFd.plugins.flags module. 
Could you write me some unit-tests for this class? I'm using pytest, but I don't want to explicitly import it. 
I also would like some docstrings per test in single quotes please.

The following code is the flags module:
import re

from CTFd.plugins import register_plugin_assets_directory


class FlagException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class BaseFlag(object):
    name = None
    templates = {}

    @staticmethod
    def compare(self, saved, provided):
        return True


class CTFdStaticFlag(BaseFlag):
    name = "static"
    templates = {  # Nunjucks templates used for key editing & viewing
        "create": "/plugins/flags/assets/static/create.html",
        "update": "/plugins/flags/assets/static/edit.html",
    }

    @staticmethod
    def compare(chal_key_obj, provided):
        saved = chal_key_obj.content
        data = chal_key_obj.data

        if len(saved) != len(provided):
            return False
        result = 0

        if data == "case_insensitive":
            for x, y in zip(saved.lower(), provided.lower()):
                result |= ord(x) ^ ord(y)
        else:
            for x, y in zip(saved, provided):
                result |= ord(x) ^ ord(y)
        return result == 0


class CTFdRegexFlag(BaseFlag):
    name = "regex"
    templates = {  # Nunjucks templates used for key editing & viewing
        "create": "/plugins/flags/assets/regex/create.html",
        "update": "/plugins/flags/assets/regex/edit.html",
    }

    @staticmethod
    def compare(chal_key_obj, provided):
        saved = chal_key_obj.content
        data = chal_key_obj.data

        try:
            if data == "case_insensitive":
                res = re.match(saved, provided, re.IGNORECASE)
            else:
                res = re.match(saved, provided)
        # TODO: this needs plugin improvements. See #1425.
        except re.error as e:
            raise FlagException("Regex parse error occured") from e

        return res and res.group() == provided


FLAG_CLASSES = {"static": CTFdStaticFlag, "regex": CTFdRegexFlag}


def get_flag_class(class_id):
    cls = FLAG_CLASSES.get(class_id)
    if cls is None:
        raise KeyError
    return cls


def load(app):
    register_plugin_assets_directory(app, base_path="/plugins/flags/assets/")

```

ChatGPT will come with the following answer:

```python
Certainly, here are some unit tests for the `CTFdRegexFlag` class using `pytest`, without the need to explicitly import it. 
The tests include docstrings as requested:

# These tests assume that the CTFdRegexFlag class and its dependencies are available.

def test_valid_regex_match_case_sensitive():
    '''
    Test a valid regex match in a case-sensitive manner using CTFdRegexFlag
    '''
    flag = CTFdRegexFlag()
    flag.content = r'^[A-Z]\d{3}$'
    flag.data = 'case_sensitive'
    provided_flag = 'A123'
    assert flag.compare(flag, provided_flag)

def test_valid_regex_match_case_insensitive():
    '''
    Test a valid regex match in a case-insensitive manner using CTFdRegexFlag
    '''
    flag = CTFdRegexFlag()
    flag.content = r'^[a-z]\d{3}$'
    flag.data = 'case_insensitive'
    provided_flag = 'A123'
    assert flag.compare(flag, provided_flag)

def test_invalid_regex_match():
    '''
    Test an invalid regex match using CTFdRegexFlag
    '''
    flag = CTFdRegexFlag()
    flag.content = r'^[A-Z]\d{3}$'
    flag.data = 'case_sensitive'
    provided_flag = 'invalid'
    assert not flag.compare(flag, provided_flag)


Make sure that you have the `CTFdRegexFlag` class and its dependencies available in your testing environment for these tests to work. These tests cover various scenarios, including valid and invalid regex matches and handling of regex parse errors.
```

## Notes

- If you exclude the unittest framework, it will assume you're using `unittest`.
- If you exclude that you don't want to explicitly import pytest, it could generate an answer with the `@pytest.mark.parametrize` decorator for parametrized tests. If you want to use parametrized test you need to import pytest to use the annotation.
- If there are test failures, you can put the error in a new ChatGPT prompt to fix it.

## Interesting articles about unit testing with AI/ChatGPT

- <https://www.codecademy.com/article/create-unit-tests-using-generative-ai>
- <https://www.codium.ai/blog/chatgpt-for-automated-testing-examples-and-best-practices/>
