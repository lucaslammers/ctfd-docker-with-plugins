# syntax=docker/dockerfile:1

# Load remote assets into the CTFd base image e.g. a custom theme or plugins
# FROM alpine/git AS git
# WORKDIR /tmp
# RUN git clone --single-branch https://github.com/0xdevsachin/CTFD-crimson-theme.git /tmp/custom-theme

FROM ctfd/ctfd:3.7.0 AS runtime

USER root

# Load assets into the CTFd base image e.g. plugins or a custom CTFd event logo
COPY ./initial_setup /opt/CTFd/CTFd/plugins/initial_setup
COPY ./container_challenges /opt/CTFd/CTFd/plugins/container_challenges
COPY ./initial_setup/HU-Logo.png /opt/CTFd/CTFd/themes/core/static/img/HU-Logo.png
# COPY ./custom_auth /opt/CTFd/CTFd/plugins/custom_auth

# COPY --from=git /tmp/custom-theme /opt/CTFd/CTFd/themes/custom-theme
COPY ./ctc2024/themes/core-beta-ctc /opt/CTFd/CTFd/themes/core-beta-ctc

USER ctfd

WORKDIR /opt/CTFd

# Ensure dependencies for CTFd plugins are installed
RUN pip install --no-cache-dir -r requirements.txt \
    && for d in CTFd/plugins/*; do \
        if [ -f "$d/requirements.txt" ]; then \
            pip install --no-cache-dir -r "$d/requirements.txt";\
        fi; \
    done;

EXPOSE 8000

ENTRYPOINT ["/opt/CTFd/docker-entrypoint.sh"]
