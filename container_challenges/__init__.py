import flask
import collections
import CTFd
import sys
import datetime

from .code import models
from .code import routes
from .code import kubernetes

from pathlib import Path

from CTFd.utils.plugins import override_template
from CTFd.plugins.migrations import upgrade

def load(app):
    dir_path = Path(__file__).parent.resolve()
    # Create database tables
    app.db.create_all()
    upgrade(plugin_name="container_challenge")

    # Register challenge type
    CTFd.plugins.challenges.CHALLENGE_CLASSES["container"] = models.ContainerChallengeType
    # Register plugin assets
    CTFd.plugins.register_plugin_assets_directory(app, base_path="/plugins/container_challenges/assets/")
    # Initialize Python Kubernetes SDK
    initialization = kubernetes.initialize()
    if initialization["status"] == False:
        error_prefix = "[ERROR] [" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
        error_suffix = "\n"
        sys.stderr.write(error_prefix + initialization["result"] + error_suffix)
    #    sys.exit(1)
    # Add pages to frontend
    Menu = collections.namedtuple("Menu", ["title", "route"])
    app.admin_plugin_menu_bar.append(Menu(title="CC Namespaces", route="/admin/container_challenges/namespaces"))
    app.admin_plugin_menu_bar.append(Menu(title="CC Configuration", route="/admin/container_challenges/configuration"))
    # Initialize Flask blueprint
    container_challenges = flask.Blueprint("container_challenges", __name__, template_folder="templates", static_folder="assets")
    # Replace admin panel and get containerID
    kubernetes.get_containerID()
    template_path = dir_path / 'templates' / 'base.html'
    override_template('admin/base.html', open(template_path).read())
    # Initialize routes
    routes.load(container_challenges)
    # Register Flask blueprint
    app.register_blueprint(container_challenges)